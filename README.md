# Notification service

Mailing service for sending messages to clients.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Usage](#usage)
- [Configuration](#configuration)
- [Testing](#testing)
- [Documentation](#documentation)
- [Contributing](#contributing)
- [License](#license)

## Prerequisites

- Docker and Docker Compose

## Installation

1. Clone the repository: git clone https://gitlab.com/azimqulovdiyorbek/notification-service.git
2. Navigate to the project directory: cd notification-service
3. Build and run the Docker containers: docker-compose up --build

## Usage

- To start the application, run `docker-compose up` and access it at `http://localhost:8000/redoc`.
- Daily emails will be automatically sent to the `ADMIN_USER` email address.

## Configuration

- The project uses the Gmail SMTP server for sending emails. Configure the following environment variables in the `.env` file:
  - `EMAIL_HOST`: SMTP server hostname
  - `EMAIL_PORT`: SMTP server port
  - `EMAIL_HOST_USER`: SMTP server username
  - `EMAIL_HOST_PASSWORD`: SMTP server password
- Timezone: The project is designed with client timezone in mind. Ensure that the necessary timezone configuration is set.

## Testing

- To run the tests, execute `pytest` in the project root directory.

## Documentation

- Detailed API documentation is available in two formats:
  - Redoc: Access the API documentation at `/redoc`.
  - Swagger: Access the API documentation at `/swagger`.

## Additional features

- Broadcast creating includes time interval which helps to target messages to client's timezone
- Daily statistics like number of sent messages and target audience.
- Documentation for APIs have been made ready. see `/redoc` and `/swagger` for different UI.
- Gitlab CI configuration file included which builds and tests the project
- Application is fully dockerized
- Few tests have been written
- Detailed logging system has been implemented for each client, mailing and message entities.
- remote server unavailability has been handled by retries and timeouts.

## Contributing

- If you encounter any issues or have suggestions for new features, please open an issue on the project's GitHub repository.
- Contributions are welcome! If you'd like to contribute code changes, please fork the repository and submit a pull request.

## License

- This project is licensed under the [MIT License](LICENSE).
