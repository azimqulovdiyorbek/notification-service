import pytest
from django.conf import settings
from mailing.services import external_message_send, send_message_if_necessary
from mailing.models import Broadcast, Client, Message
from django.utils import timezone


@pytest.fixture
def broadcast():
    return Broadcast.objects.create(
        distribution_properties={
            "tag": "YOUNGSTERS",
            "mobile_operator_code": "MEGAFON",
        },
        message_text="Test message",
        time_interval={
            "start_time": "12:00",
            "end_time": "18:00",
        },
        launch_datetime=timezone.datetime.strptime("2023-05-10T10:00:00+00:00",
                                                   "%Y-%m-%dT%H:%M:%S%z"),
        end_datetime=timezone.datetime.strptime("2023-08-10T11:00:00+00:00",
                                                "%Y-%m-%dT%H:%M:%S%z"),
    )


@pytest.fixture
def client():
    return Client.objects.create(
        phone_number="1234567890",
        mobile_operator_code="MEGAFON",
        tag="YOUNGSTERS",
        timezone="Europe/Moscow",
    )


@pytest.mark.django_db
def test_message_created_after_broadcast_execution(broadcast, client):
    send_message_if_necessary(broadcast)
    assert Message.objects.count() == 1
    message = Message.objects.first()
    assert message.broadcast == broadcast
    assert message.client == client
