from typing import Iterable
from copy import deepcopy
from django.db import models
from django.utils import timezone
from enum import Enum
import datetime


class Event(Enum):
    READ = 'read'
    CREATE = 'create'
    UPDATE = 'update'
    DELETE = 'delete'
    SEND = 'send'
    SCHEDULE = 'schedule'
    EXECUTE = 'execute'
    REVOKE = 'revoke'


def convert_str_time_to_time(time: str) -> datetime.time:
    """
    time: "12:11", -> datetime.time(12, 11)
    """
    hour, min = time.split(':')
    return datetime.time(int(hour), int(min))


def get_dict_from_model(model):
    model_dict = deepcopy(model.__dict__)
    if '_state' in model_dict:
        del model_dict['_state']
    for key, value in model_dict.items():
        if isinstance(value, datetime.datetime):
            model_dict[key] = str(value)
    return model_dict


def current_datetime():
    current_time = timezone.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S%z')
    current_time = timezone.datetime.strptime(formatted_time,
                                              '%Y-%m-%d %H:%M:%S%z')
    return current_time
