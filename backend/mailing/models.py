from django.db import models


class Client(models.Model):
    phone_number = models.CharField(max_length=11)
    mobile_operator_code = models.CharField(max_length=16)
    tag = models.CharField(max_length=256)
    timezone = models.CharField(max_length=128)


class Broadcast(models.Model):
    launch_datetime = models.DateTimeField()
    message_text = models.TextField()
    distribution_properties = models.JSONField(default={})
    time_interval = models.JSONField(default={})
    end_datetime = models.DateTimeField()


class Message(models.Model):
    status_choices = [('sent', 'sent'), ('failed', 'failed')]
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=16, choices=status_choices)
    broadcast = models.ForeignKey(Broadcast, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)


class BackgroundBroadcast(models.Model):
    broadcast = models.ForeignKey(Broadcast, on_delete=models.CASCADE)
    background_task_id = models.CharField(max_length=256)


class Log(models.Model):
    """
    logging system which is used to log all the actions
    that are performed by the system.
    it should be json field so not constraint at all.
    it should include what action has been performed for which model
    when it was done
    log: {
        "action": "create",
        "model": "broadcast",
        "created_at": "2021-01-01 00:00:00",
        "data": {
            "launch_datetime": "2021-01-01 00:00:00",
            "message_text": "hello world",
            "distribution_properties": {},
            "time_interval": {},
            "end_datetime": "2021-01-01 00:00:00"
            }

    }
    """
    created_at = models.DateTimeField(auto_now_add=True)
    log = models.JSONField(default={})
