import pytz
import time
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.db.models import Count
import requests
from celery.result import AsyncResult
from django.conf import settings
from notification_service.celery import app
from .models import Broadcast, Message, Client

from .api import (
    create_background_broadcast,
    update_message,
    create_message,
    get_clients,
    get_broadcast_by_id,
    get_client_by_id,
    task_ids_by_broadcast,
)
from .api import create_log
from .utils import convert_str_time_to_time, get_dict_from_model, Event, current_datetime


@app.task
def send_message_in_time(broadcast_id):
    broadcast = get_broadcast_by_id(broadcast_id)
    send_message(broadcast)


@app.task
def schedule_send_message_to_client(client_id, broadcast_id):
    broadcast = get_broadcast_by_id(broadcast_id)
    client = get_client_by_id(client_id)
    send_message_to_client(client, broadcast)
    create_log(
        Event.SEND, 'client',
        [get_dict_from_model(instance) for instance in [client, broadcast]])


def destroy_scheduled_message_sending(broadcast: Broadcast) -> None:
    task_ids = task_ids_by_broadcast(broadcast)
    for task_id in task_ids:
        task = AsyncResult(task_id)
        task.revoke()
        create_log(Event.REVOKE, 'task', {'task_id': task_id})


def send_message_if_necessary(broadcast: Broadcast):
    current_time = current_datetime()
    if current_time > broadcast.launch_datetime \
       and current_time < broadcast.end_datetime:
        send_message(broadcast)
        create_log(Event.SEND, 'broadcast', get_dict_from_model(broadcast))

    elif current_time < broadcast.launch_datetime:
        task_id = send_message_in_time.apply_async(
            args=(broadcast.id, ),
            eta=broadcast.launch_datetime,
        )
        create_background_broadcast(task_id, broadcast)
        create_log('schedule', 'broadcast', get_dict_from_model(broadcast))


def external_message_send(message_id: int, phone: str, text: str) -> bool:
    headers = {
        'accept': 'application/json',
        'Authorization': f'Bearer {settings.SEND_SERVICE_API_TOKEN}',
        'Content-Type': 'application/json',
    }
    data = {
        'id': message_id,
        'phone': phone,
        'text': text,
    }
    url = settings.SEND_SERVICE_API_URL + str(message_id)
    for retry in range(settings.MAX_RETRIES):
        try:
            response = requests.request(
                'POST',
                url=url,
                headers=headers,
                json=data,
                timeout=5,
            )
            if response.status_code != 200:
                return False
            return True
        except requests.Timeout:
            pass
        except requests.RequestException as e:
            pass

        time.sleep(settings.RETRY_DELAY)
    return False


def send_message(broadcast: Broadcast):
    current_time = current_datetime()
    if broadcast.end_datetime < current_time:
        return
    filter = {}
    tag = broadcast.distribution_properties.get('tag')
    mobile_operator_code = broadcast.distribution_properties.get(
        'mobile_operator_code')
    if tag:
        filter['tag'] = tag
    if mobile_operator_code:
        filter['mobile_operator_code'] = mobile_operator_code
    clients = get_clients(filter)
    for client in clients:
        client_timezone = pytz.timezone(client.timezone)
        current_time = current_datetime()
        client_time = current_time.astimezone(client_timezone)
        start_time = convert_str_time_to_time(
            broadcast.time_interval['start_time'])
        end_time = convert_str_time_to_time(
            broadcast.time_interval['end_time'])
        start_datetime = client_time.replace(hour=start_time.hour,
                                             minute=start_time.minute)
        end_datetime = client_time.replace(hour=end_time.hour,
                                           minute=end_time.minute)
        if client_time > start_datetime and client_time < end_datetime:
            create_log(Event.SEND, 'client', get_dict_from_model(client))
            send_message_to_client(client, broadcast)
        else:
            create_log('schedule', 'client', get_dict_from_model(client))
            schedule_send_message_to_client.apply_async(
                args=(client.id, broadcast.id),
                eta=start_datetime,
            )
    create_log(Event.SEND, 'broadcast', get_dict_from_model(broadcast))


def execute_broadcast():
    current_time = current_datetime()
    active_broadcasts = Broadcast.objects.filter(
        launch_datetime__lt=current_time, end_datetime__gt=current_time)
    for broadcast in active_broadcasts:
        send_message(broadcast)
        create_log(Event.SEND, 'broadcast', get_dict_from_model(broadcast))
    create_log(
        'execute', 'broadcast',
        [get_dict_from_model(broadcast) for broadcast in active_broadcasts])


def send_message_to_client(client, broadcast):
    message = create_message(
        broadcast=broadcast,
        client=client,
    )
    create_log(Event.CREATE, 'message', get_dict_from_model(message))
    message_sent = external_message_send(
        message_id=message.id,
        phone=client.phone_number,
        text=broadcast.message_text,
    )
    create_log(
        Event.SEND, 'external_message', {
            'message_id': message.id,
            'phone': client.phone_number,
            'text': broadcast.message_text,
        })
    if not message_sent:
        data = {'status': 'failed'}
        update_message(message, data)
        create_log(Event.UPDATE, 'message', get_dict_from_model(message))


def get_daily_statistics():
    current_time = current_datetime()
    messages = Message.objects.filter(
        created_at__year=current_time.year,
        created_at__month=current_time.month,
        created_at__day=current_time.day,
    )

    tag_stats = Client.objects.values('tag').annotate(
        count=Count('message')).order_by('-count')
    operator_stats = Client.objects.values('mobile_operator_code').annotate(
        count=Count('message')).order_by('-count')
    result = {
        'tag_stats': list(tag_stats),
        'operator_stats': list(operator_stats),
        'additonal_data': {
            'success_count': messages.filter(status='sent').count(),
            'total_messages': messages.count(),
            'fail_count': messages.filter(status='failed').count(),
        }
    }
    create_log(Event.READ, 'daily_statistics', result)
    return result


def send_email(subject, message, recipient_list):
    from_email = settings.EMAIL_HOST_USER
    to_email = recipient_list

    email = EmailMessage(subject=subject,
                         body=message,
                         from_email=from_email,
                         to=to_email)
    email.content_subtype = 'html'
    try:
        email.send()
        create_log(
            Event.SEND, 'email', {
                'subject': subject,
                'message': message,
                'recipient_list': recipient_list,
            })
    except Exception as e:
        create_log(
            Event.SEND, 'email', {
                'subject': subject,
                'message': message,
                'recipient_list': recipient_list,
                'error': str(e),
            })


@app.task
def report_daily_statistics():
    statistics = get_daily_statistics()
    subject = 'Daily statistics'
    message = render_to_string('./daily_statistics.html', statistics)
    recipient_list = [settings.ADMIN_EMAIL]
    send_email(subject, message, recipient_list)
