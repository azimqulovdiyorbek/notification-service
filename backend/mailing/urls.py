from django.urls import path
from .views import (
    ClientView,
    BroadcastView,
    general_statistics_view,
    single_statistics_view,
    execute_broadcast_view,
    get_logs_by_model_id_view,
)

urlpatterns = [
    path('clients', ClientView.as_view(), name='clients'),
    path('clients/<int:pk>', ClientView.as_view(), name='clients'),
    path('broadcasts', BroadcastView.as_view(), name='broadcasts'),
    path('broadcasts/<int:pk>', BroadcastView.as_view(), name='broadcasts'),
    path('broadcasts/execute', execute_broadcast_view),
    path('statistics', general_statistics_view),
    path('statistics/<int:pk>', single_statistics_view),
    path('logs/', get_logs_by_model_id_view),
]
