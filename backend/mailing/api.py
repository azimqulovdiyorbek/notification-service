from copy import deepcopy
from datetime import datetime
from .models import BackgroundBroadcast, Broadcast, Message, Client, Log


def get_broadcast_by_id(id: int):
    return Broadcast.objects.get(id=id)


def get_client_by_id(id: int):
    return Client.objects.get(id=id)


def create_background_broadcast(task_id: str, broadcast: Broadcast) -> None:
    BackgroundBroadcast.objects.create(
        broadcast=broadcast,
        background_task_id=task_id,
    )


def create_message(broadcast: Broadcast, client: Client):
    return Message.objects.create(
        broadcast=broadcast,
        client=client,
        status='sent',
    )


def update_message(message: Message, data: dict) -> None:
    for field, value in data.items():
        setattr(message, field, value)
    message.save()


def get_clients(filter: dict):
    if len(filter) != 0:
        return Client.objects.filter(**filter)
    return Client.objects.all()


def task_ids_by_broadcast(broadcast: Broadcast) -> list[str]:
    return list(
        BackgroundBroadcast.objects.filter(
            broadcast=broadcast).values_list('background_task_id'))


def create_log(action, model, data):
    """
    function to create log
    """
    log = {
        "action": str(action),
        "model": model,
        "created_at": str(datetime.now()),
        "data": data
    }
    Log.objects.create(log=log)


def get_logs_by_model_id(model: str, id: int):
    logs = Log.objects.filter(log__model=model, log__data__id=int(id))
    result = []
    for log in logs:
        result.append(log.log)
    return result


def get_dict_from_model(model):
    model_dict = deepcopy(model.__dict__)
    if '_state' in model_dict:
        del model_dict['_state']
    for key, value in model_dict.items():
        if isinstance(value, datetime.datetime):
            model_dict[key] = str(value)
    return model_dict
