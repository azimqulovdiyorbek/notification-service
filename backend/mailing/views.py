from rest_framework.generics import (CreateAPIView, UpdateAPIView,
                                     DestroyAPIView)
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .serializers import ClientSerializer, BroadcastSerializer
from .models import Client, Broadcast, Message
from .services import send_message_if_necessary, destroy_scheduled_message_sending, execute_broadcast
from .api import create_log, get_logs_by_model_id
from .utils import get_dict_from_model, Event


class ClientView(CreateAPIView, UpdateAPIView, DestroyAPIView):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()

    def perform_create(self, serializer):
        serializer.save()
        create_log(Event.CREATE, 'client', serializer.data)

    def perform_destroy(self, instance):
        data = get_dict_from_model(instance)
        create_log(Event.DELETE, 'client', data)
        super().perform_destroy(instance)

    def perform_update(self, serializer):
        serializer.save()
        create_log(Event.UPDATE, 'client', serializer.data)


class BroadcastView(CreateAPIView, UpdateAPIView, DestroyAPIView):
    serializer_class = BroadcastSerializer
    queryset = Broadcast.objects.all()

    def perform_create(self, serializer):
        instance = serializer.save()
        create_log(Event.CREATE, 'broadcast', serializer.data)
        send_message_if_necessary(instance)

    def perform_destroy(self, instance):
        data = get_dict_from_model(instance)
        create_log(Event.DELETE, 'broadcast', data)
        destroy_scheduled_message_sending(instance)
        super().perform_destroy(instance)

    def perform_update(self, serializer):
        instance = serializer.save()
        create_log(Event.UPDATE, 'broadcast', serializer.data)
        destroy_scheduled_message_sending(instance)
        send_message_if_necessary(instance)


@api_view(['GET'])
def general_statistics_view(request):
    broadcasts = Broadcast.objects.all()
    broadcast_info = []
    for broadcast in broadcasts:
        info = {
            'broadcast_id':
            broadcast.id,
            'broadcast_launch':
            str(broadcast.launch_datetime),
            'broadcast_end':
            str(broadcast.end_datetime),
            'messages':
            Message.objects.filter(broadcast=broadcast).count(),
            'success_count':
            Message.objects.filter(broadcast=broadcast, status='sent').count(),
            'fail_count':
            Message.objects.filter(broadcast=broadcast,
                                   status='failed').count()
        }
        broadcast_info.append(info)
    result = {
        'total_messages': Message.objects.count(),
        'success_count': Message.objects.filter(status='sent').count(),
        'fail_count': Message.objects.filter(status='failed').count(),
        'broadcast_info': broadcast_info
    }
    create_log(Event.READ, 'general_statistics', result)
    return Response(result, status=status.HTTP_200_OK)


@api_view(['GET'])
def single_statistics_view(request, pk):
    broadcast = Broadcast.objects.filter(id=pk)
    if not broadcast:
        return Response(
            {
                'result': 'fail',
                'message': 'Resource could not be found!'
            },
            status=status.HTTP_400_BAD_REQUEST)
    broadcast = broadcast.get()
    messages = Message.objects.filter(broadcast=broadcast).values()
    result = {
        'launch_datetime': str(broadcast.launch_datetime),
        'end_datetime': str(broadcast.end_datetime),
        'message_text': broadcast.message_text,
        'distribution_properties': broadcast.distribution_properties,
        'number_of_clients_n_messages': len(messages),
        'messages': messages
    }
    create_log(Event.READ, 'single_statistics', result)
    return Response(result, status=status.HTTP_200_OK)


@api_view(['POST'])
def execute_broadcast_view(request):
    execute_broadcast()
    create_log(Event.EXECUTE, 'broadcast', {})
    return Response(
        {
            'result': 'success',
            'message': 'Messages have been broadcasted!'
        },
        status=status.HTTP_201_CREATED)


@api_view(['GET'])
def get_logs_by_model_id_view(request):
    model = request.query_params.get('model')
    id = request.query_params.get('id')
    if not model and not id:
        return Response(
            {
                'result': 'fail',
                'message': 'Resource could not be found!'
            },
            status=status.HTTP_400_BAD_REQUEST)
    logs = get_logs_by_model_id(model, id)
    create_log(Event.READ, 'logs', logs)
    return Response(logs, status=status.HTTP_200_OK)
