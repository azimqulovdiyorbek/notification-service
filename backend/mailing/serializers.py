import re
import pytz
from datetime import datetime
from rest_framework import serializers
from .models import Client, Broadcast


def validate_phone_number(phone_number):
    regex = r'^7\d{10}$'
    if not re.match(regex, phone_number):
        raise serializers.ValidationError(
            f'Phone number {phone_number} is not valid!')


def validate_time_zone(timezone):
    if timezone in pytz.all_timezones is False:
        raise serializers.ValidationError(
            f'timezone {timezone} is not supported!')


def validate_filter_properties(filter_properties):
    if 'tag' not in filter_properties or \
       'mobile_operator_code' not in filter_properties:
        raise serializers.ValidationError(
            '`tag` or `mobile_operator_code` must be included!')


def validate_time_interval(time_interval):
    if 'start_time' not in time_interval and \
       'end_time' not in time_interval:
        raise serializers.ValidationError(
            '`start_time` and `end_time` must be included!')
    start_time = time_interval['start_time']
    end_time = time_interval['end_time']
    try:
        datetime.strptime(start_time, '%H:%M')
        datetime.strptime(end_time, '%H:%M')
    except ValueError as exc:
        raise serializers.ValidationError(str(exc))


class ClientSerializer(serializers.ModelSerializer):

    phone_number = serializers.CharField(validators=[validate_phone_number])
    timezone = serializers.CharField(validators=[validate_time_zone])

    class Meta:
        model = Client
        fields = '__all__'


class BroadcastSerializer(serializers.ModelSerializer):
    distribution_properties = serializers.JSONField(
        validators=[validate_filter_properties], required=False)
    time_interval = serializers.JSONField(validators=[validate_time_interval])

    class Meta:
        model = Broadcast
        fields = '__all__'
